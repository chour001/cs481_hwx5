﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace Homework5
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();

        }
        async void OnButtonClicked(object sender, EventArgs args)
        {
            map.MapType = Xamarin.Forms.Maps.MapType.Satellite;
        }

        async void OnButtonClickedStreet(object sender, EventArgs args)
        {
            map.MapType = Xamarin.Forms.Maps.MapType.Street;
        }

        void OnPickerSelectedIndexChanged(object sender, EventArgs e)
        {
            var picker = (Picker)sender;
            int selectedIndex = picker.SelectedIndex;

            if (selectedIndex != -1)
            {
                if ((string)picker.ItemsSource[selectedIndex] == "San Francisco")
                {
                    var SFLocation = MapSpan.FromCenterAndRadius(new Position(37.787994, -122.407437), Distance.FromMiles(3));
                    map.MoveToRegion(SFLocation);
                }

                if ((string)picker.ItemsSource[selectedIndex] == "Santa Cruz")
                {
                    var STLocation = MapSpan.FromCenterAndRadius(new Position(36.9628066, -122.0194722), Distance.FromMiles(3));
                    map.MoveToRegion(STLocation);
                }

                if ((string)picker.ItemsSource[selectedIndex] == "Los Angeles")
                {
                    var LALocation = MapSpan.FromCenterAndRadius(new Position(34.003342, -118.485832), Distance.FromMiles(3));
                    map.MoveToRegion(LALocation);
                }
                if ((string)picker.ItemsSource[selectedIndex] == "San Diego")
                {
                    var SDLocation = MapSpan.FromCenterAndRadius(new Position(32.715738, -117.1610838), Distance.FromMiles(3));
                    map.MoveToRegion(SDLocation);
                }


            }
        }
    }
}